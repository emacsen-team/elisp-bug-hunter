elisp-bug-hunter (1.3.1+repack-10) unstable; urgency=medium

  [ Xiyue Deng ]
  * Team upload.
  * Sync and refresh patches using gbp-pq
  * Update patch to accommodate output change since Emacs 30.1
  * Clean-up left-over test files to fix build-twice-in-a-row

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 28 Feb 2025 20:13:13 +0800

elisp-bug-hunter (1.3.1+repack-9) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 19:04:11 +0900

elisp-bug-hunter (1.3.1+repack-8) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.3.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 24 Jul 2024 22:03:01 +0900

elisp-bug-hunter (1.3.1+repack-7) unstable; urgency=medium

  [ Aymeric Agon-Rambosson ]
  * Adapt d/p/fix-tests-emacs-28.patch with emacs 29 (Closes: #1052926).

  [ Lev Lamberov ]
  * Add upstream metadata
  * d/control: Clean Depends
  * d/control: Declare Standards-Version 4.6.2 (no changes needed)

 -- Lev Lamberov <dogsleg@debian.org>  Sun, 29 Oct 2023 12:37:49 +0500

elisp-bug-hunter (1.3.1+repack-6) unstable; urgency=medium

  * Team upload
  * Add patch to repair tests with emacs 28 (Closes: #1020195)
  * d/control: Declare Standards-Version 4.6.1 (no changes needed)

 -- Aymeric Agon-Rambosson <aymeric.agon@yandex.com>  Sat, 21 Jan 2023 14:26:47 +0100

elisp-bug-hunter (1.3.1+repack-5) unstable; urgency=medium

  [ David Krauser ]
  * Update maintainer email address

  [ Lev Lamberov ]
  * Build against newer dh_elpa
  * Migrate to dh 13 without d/compat
  * d/control: Declare Standards-Version 4.5.1 (no changes needed)
  * d/control: Add Rules-Requires-Root: no
  * d/control: Drop emacs{24,25} from Enhances
  * d/control: Drop Built-Using field
  * d/copyright: Bump copyright years

 -- Lev Lamberov <dogsleg@debian.org>  Fri, 11 Dec 2020 17:25:05 +0500

elisp-bug-hunter (1.3.1+repack-4) unstable; urgency=medium

  * Team upload.
  * Regenerate source package with quilt patches

 -- David Bremner <bremner@debian.org>  Sun, 25 Aug 2019 14:22:19 -0300

elisp-bug-hunter (1.3.1+repack-3) unstable; urgency=medium

  * Team upload.
  * Rebuild with current dh-elpa

 -- David Bremner <bremner@debian.org>  Sat, 24 Aug 2019 20:34:58 -0300

elisp-bug-hunter (1.3.1+repack-2) unstable; urgency=medium

  * Team upload.
  * Rebuild with dh-elpa 1.13 to fix byte-compilation with unversioned
    emacs

 -- David Bremner <bremner@debian.org>  Fri, 01 Jun 2018 20:56:10 -0300

elisp-bug-hunter (1.3.1+repack-1) unstable; urgency=medium

  * Initial release (Closes: #855388)

 -- Lev Lamberov <dogsleg@debian.org>  Fri, 17 Feb 2017 16:46:10 +0500
